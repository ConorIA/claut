#' Get timezone (UTSC offset) from the lutz package
#'
#' @param lat numeric; the latitude of the point for which to find a timezone
#' @param lon numeric; the longitude of the point for which to find a timezone
#' @param ignore_dst Boolean; whether to ignore DST (return only standard time)
#'
#' @importFrom tibble tibble
#' @importFrom dplyr %>% filter select
#'
#' @export

get_tz <- function(lat, lon, ignore_dst = TRUE) {
  if (requireNamespace("lutz", quietly = TRUE)) {
    tibble(lutz::tz_list()) %>%
      filter(tz_name == lutz::tz_lookup_coords(lat, lon, method = "accurate")) -> tz
    tz <- if (nrow(tz) > 1 & any(tz$is_dst)) {
      filter(tz, is_dst == !ignore_dst)
    } else {
      tz[1,]
    }
    tz %>%
      select(utc_offset_h) %>%
      unlist() %>%
      unname()
  } else {
    message("This function requires the lutz package. Please install lutz.")
  }
}
