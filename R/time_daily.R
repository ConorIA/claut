#' Daily date from hourly using a UTC time cutoff
#'
#' Calculates daily maxima and minima using a UTC time cutoff
#'
#' @param dat data frame; the data to analyze; the first column must be date times; subsequent columns should contain for which to detect extremes
#' @param tz integer; the number of hours shifted from UTC, e.g. for UTC-5, pass \code{-5}
#' @param cutoff_time integer from 0-24; the hour UTC at which one day becomes the next; defaults to 6 (i.e. 6 AM UTC)
#' @param na.rm Boolean; whether to pass \code{na.rm = TRUE} to the aggregation functions ({\link{max}}, {\link{mean}}, \code{\link{min}})
#'
#' @author Conor I. Anderson
#'
#' @return A table of class \code{\link{tbl_df}}.
#'
#' @importFrom dplyr %>% across case_when count everything filter mutate select starts_with summarize
#' @importFrom lubridate date days force_tz with_tz
#' @importFrom rlang .data
#' @importFrom tibble tibble
#' @importFrom tidyselect peek_vars
#'
#' @export
#'

time_daily <- function(dat, tz, cutoff_time = 6, na.rm = FALSE) {

  if (names(dat)[1] != "DateTime") names(dat)[1] <- "DateTime"

  dat %>% mutate(DateTime = force_lst(DateTime, tz),
                 DateTimeUTC = with_tz(.data$DateTime, "UTC"),
                 Date = case_when(
                   hour(DateTimeUTC) < cutoff_time ~ date(DateTimeUTC) - days(1),
                   TRUE ~ date(DateTimeUTC))) %>%
    select(.data$Date, !starts_with("Date")) -> dat

  agg <- function(x, f, na.rm = FALSE) {
    if (all(is.na(x))) {
      NA
    } else {
      get(f)(x, na.rm = na.rm)
    }
  }

  counts <- dat %>% group_by(.data$Date) %>% count()

  missing <- dat %>%
    group_by(.data$Date) %>%
    summarize(across(.cols = everything(), .fns = list(Missing = ~sum(is.na(.x)))))

  missing <- left_join(counts, missing, by = "Date")

  dat %>%
    group_by(Date)  %>%
    summarize(across(.cols = everything(),
                     .fns = list(Max = ~agg(.x, "max", na.rm = na.rm),
                                 Mean = ~agg(.x, "mean", na.rm = na.rm),
                                 Min = ~agg(.x, "min", na.rm = na.rm)),
                     .names = "{.fn}{.col}"),
              .groups = "drop") %>%
    select(sort(peek_vars())) -> dat

  if (na.rm) {
    # When using 6 UTC, west coast cities will have start dates with two hours missing
    # because each "day" ends at 10 PM Vancouver time
    dat[(counts$n < 22), 2:ncol(dat)] <- NA
  } else {
    dat[(counts$n < 24), 2:ncol(dat)] <- NA
  }

  attr(dat, "missing") <- missing

  dat
}
